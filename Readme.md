# KoyfinTestsTask

RestAPI server for assets handling;

## Contents

- [Dependencies](#dependencies)
- [Preparing Application](#preparing-application)
- [Build Images](#build-images)
- [Start application](#start-application)
- [Usage](#usage)

## Dependencies

* Nodejs 8.9.3
* npm
* docker
* docker-compose

## Preparing Application

Clone this project to the common place. For example:

```sh
$ git clone git@gitlab.com:sir_ingvarr/koyfin-test-task.git ~/workspace/assetsAPI 
```

## Build Images

```sh
$ cd ~/workspace/assetsAPI
$ docker build -t api:koyfin .
$ cd ./mongoDocker
$ docker build -t mongo:koyfin .                                                          
```

## Start application

Make sure PORT 9988 (can be changed in .env.docker config file) and 27017 (default mongo) are free for use.

```sh
$ cd ~/workspace/assetsAPI
$ docker-compose up -d
```

## Usage

After start of server it can be accessed from localhost:9988. Base of API is /api/v0.
Detailed documentation can be seen in rest.yml
