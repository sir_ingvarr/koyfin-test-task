module.exports = {
	apps : [{
		name   : 'assets',
		script : './runProd',
		env: {
			'ENV': 'docker',
		},
		watch: true,
		ignore_watch: ['uploads']
	}]
};