import server from './bin/server';

const { PORT } = process.env;

server.listen({
	port: PORT,
},

() => {
	console.log(`SYS::STARTED SERVER ON :${PORT}`);
}
);
