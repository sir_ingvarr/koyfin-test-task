import Koa from 'koa';
import helmet from 'koa-helmet';
const bodyParser = require('koa-bodyparser');

const app = new Koa();

app.use(helmet());
app.use(bodyParser());

import initRoutes from '../middleware/router';

app.use(async (ctx, next) => {
	const start = Date.now();
	await next();
	const ms = Date.now() - start;
	console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

initRoutes(app);

export default app;
