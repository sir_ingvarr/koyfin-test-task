import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import schemas from './schemas/index';


class Database {
	constructor() {
		try {
			const { DB_HOST, DB_PORT, DB_NAME } = process.env;
			const databaseURI = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`;
			this._database = mongoose.connect(databaseURI);
			this._initModels(schemas);
		}
		catch(err) {
			console.log(err);
		}
	}

	_initModels(schemas) {
		for (let entity in schemas) {
			this[entity] = mongoose.model(
				entity,
				new mongoose.Schema(schemas[entity]).plugin(uniqueValidator)
			);
		}
	}
}


export default new Database();
