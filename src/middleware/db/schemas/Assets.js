export const assets = {
	id: {type: String, required: true},
	size: {type: Number, required: true, default: 0},
	contentType: {type: String, required: false},
	content: {type: String, required: false}
};
