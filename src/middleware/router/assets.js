import Router from 'koa-router';
import { 
	getAssetById,
	createNewAsset, 
	updateAssetById, 
	getAssetsList, 
	getAssetContentById,
} from '../handlers/assets';
import { upload } from '../storage';
import { ensureComplicity, sendNotFound } from './helpers';


const { API_BASE } = process.env;
const assetsRouter = new Router();

assetsRouter.get('/', async (ctx, next) => {
	await ensureComplicity(async () => {
		const res = await getAssetsList();
		ctx.response.body = res;
		await next();
	}, ctx);
});

assetsRouter.get('/:id', async (ctx, next) => {
	await ensureComplicity(async () => {
		const { id } = ctx.params;
		const res = await getAssetById(id);
		if(res) {
			ctx.response.status = 200;
			ctx.response.body = res;
			await next();
		} else {
			sendNotFound(ctx, next);
		}
	}, ctx);
});

assetsRouter.post('/', async (ctx, next) => {
	await ensureComplicity(async () => {
		const res = await createNewAsset();
		ctx.response.status = 201;
		ctx.response.body = `${API_BASE}/assets/${res}`;
		await next();
	}, ctx);
});

assetsRouter.put('/:id/content', upload.single('upfile'), async (ctx, next) => {
	await ensureComplicity(async () => {
		if(!ctx.req.file) {	
			ctx.response.status = 400;
			ctx.response.body = {code: 400, message: 'missing file'};
			return await next();
		}
		const {destination, filename, size, mimetype} = ctx.req.file;
		const { id } = ctx.params;
		const res = await updateAssetById(id, `${destination}/${filename}`, size, mimetype);
		if(res) {
			ctx.response.status = 204;
			ctx.response.message = 'Ok';
			await next();
		} else {
			sendNotFound(ctx, next);
		}
	}, ctx);
});
assetsRouter.get('/:id/content', async (ctx, next) => {
	await ensureComplicity(async () => {
		const { id } = ctx.params;
		const res = await getAssetContentById(ctx, id);
		next();
		if(!res) {
			sendNotFound(ctx, next);
		}
	}, ctx);
});

export default assetsRouter;
