export const ensureComplicity = async (func, ctx) => {
	try {
		await func();
	} catch (err) {
		console.error(err);
		ctx.throw(500, {message: 'Internal Server Error'});
	}
};

export const sendNotFound = (ctx, next) => {
	ctx.response.status = 404;
	ctx.response.body = {code: 404, message: 'Asset not found'};
	next();
};
