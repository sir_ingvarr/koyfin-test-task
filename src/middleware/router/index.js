const routers = [];
routers.push(require('./api').default);

module.exports = (server) => {
	return routers.map((router) => server.use(router.routes()));
};
