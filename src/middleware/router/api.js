import apiRouter from 'koa-router';
import assetsRouter from './assets';
const router = new apiRouter({prefix: '/api/v0'});

router.use('/assets', assetsRouter.routes());

export default router;
