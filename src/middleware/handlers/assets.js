import db from '../db/Database';
import uuid from 'uuid/v4';
import fs from 'fs';
import send from 'koa-send';

export const createNewAsset = async () => {
	try {
		const uid = uuid();
		const asset = db.assets({ id: uid });
		await asset.save();
		return uid;
	} catch (err) {
		console.error(err);
	}
};

export const getAssetsList = async () => {
	try {
		const res = await db.assets.find().select('id').exec();
		return res.reduce((acc, val) => [...acc, val.id], []);
	} catch (err) {
		console.error(err);
	}
};

export const getAssetById = async (id) => {
	try {
		const res = await db.assets.findOne({id}).select('id size contentType').exec();
		return res && { id: res.id, res: res.contentType, size: res.size } || false;
	} catch (err) {
		console.error(err);
	}
};

// simplified upload path resolving on purpose =)

export const updateAssetById = async (id, content, size, contentType) => {
	try {
		const res = await db.assets.findOneAndUpdate(
			{id}, 
			{ $set: { content, size, contentType }}, 
			{ new: false })
			.exec();
		if(!res) {
			fs.unlink(`${__dirname}/../../../${content}`, () => {});
			return false;
		}
		return true;
	} catch (err) {
		console.error(err);
	}
};

export const getAssetContentById = async (ctx, id) => {
	try {
		const res = await db.assets.findOne({id}).select('content').exec();
		if(res) return send(ctx, `${res.content}`);
		else return false;
	} catch (err) {
		console.error(err);
	}
};

