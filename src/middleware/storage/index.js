import multer from 'koa-multer';
import fs from 'fs';

const uploadsPath = `${__dirname}/../../../uploads`;

try {
	fs.statSync(uploadsPath);
} catch (e) {
	fs.mkdirSync(uploadsPath);
}

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'uploads');
	},
	filename: function (req, file, cb) {
		cb(null, `${Date.now()}-${file.originalname}`);
	}
});

export const upload = multer({ storage });