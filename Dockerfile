FROM node:8.10.0-stretch
EXPOSE 9988
COPY ./ /var/www/
WORKDIR /var/www/
RUN npm install
ENTRYPOINT npm run startPROD && npm run logs
