const dotenv = require('dotenv-safe');
const path = require('path');
const request = require('request-promise');
const { expect } = require('chai');
const fs = require('fs');
require('babel-polyfill');

const ENV  = process.env.ENV || 'local';

dotenv.config({
	allowEmptyValues: true,
	path: path.resolve('config/.env.'+ENV),
});

const { API_BASE, PORT } = process.env;

const testedApi = `http://localhost:${PORT}${API_BASE}/assets`;


require('../../src');

const options = {
	method: 'GET',
	uri: testedApi,
	json: true,
	resolveWithFullResponse: true
};

describe('end-to-end tests of restapi', () => {
	let assetId;

	it('check the get /assets request', async () => {
		const res = await request(options);
		expect(res.statusCode).to.equal(200);
		expect(res.headers['content-type']).to.contain('application/json');
		expect(res.body).to.be.an('array');
	});

	it('check the post /assets request', async () => {
		const opts = { ...options, ...{ method: 'POST' }};
		const res = await request(opts);
		expect(res.statusCode).to.be.equal(201);
		expect(res.body).to.be.a('string');
		assetId = res.body.split('/')[4];
	});

	it('check the get /assets/:id request success', async () => {
		const opts = { ...options, ...{ uri: options.uri + '/' + assetId } };
		const res = await request(opts);
		expect(res.statusCode).to.be.equal(200);
		expect(res.body).to.be.an('object').that.ownProperty('size').that.equals(0);
	});

	it('check the get /assets/:id request not found', async () => {
		const opts = { ...options, ...{ uri: options.uri + '/' + 123, simple: false } };
		const res = await request(opts);
		expect(res.statusCode).to.be.equal(404);
		expect(res.body).to.be.an('object').that.ownProperty('message').that.equals('Asset not found');
	});

	it('check the put /assets/:id/content request success', async () => {
		const opts = { 
			...options, 
			...{ 
				method: 'PUT', 
				uri: options.uri + '/' + assetId + '/content',
				formData: {upfile: fs.createReadStream(__dirname + '/../fixture/immafile.jpeg') }
			}
		};
		const res = await request(opts);
		expect(res.statusCode).to.be.equal(204);
	});

	it('check the get /assets/:id/content request success', async () => {
		const opts = { 
			...options, 
			...{ 
				method: 'GET', 
				uri: options.uri + '/' + assetId + '/content'
			}
		};
		const res = await request(opts);
		expect(res.statusCode).to.be.equal(200);
		expect(res.headers['content-type']).to.contain('image/jpeg');
	});
});

